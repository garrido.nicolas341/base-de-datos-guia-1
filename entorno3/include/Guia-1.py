from pymongo import MongoClient # El cliente de MongoDB
from bson.objectid import ObjectId # Para crear ObjectId, porque _id como cadena no funciona

class Licor:
    def __init__(self, nombre, precio):
        self.nombre = nombre
        self.precio = precio

def conexion():
    db  = "Licores"
    cliente = MongoClient('localhost:27017')
    return cliente[db]

def insertar(licor):
    db = conexion()
    licors = db.licors
    return licors.insert_one({
        "Nombre Licor": licor.nombre,
        "Precio Licor": licor.precio,
        })

def obtener():
    db = conexion()
    return db.licors.find()

def actualizar(id, licor):
    db = conexion()
    resultado = db.licors.update_one(
        {
        '_id': ObjectId(id)
        }, 
        {
            '$set': {
                "Nombre Licor": licor.nombre,
                "Precio Licor": licor.precio,
            }
        })
    return resultado

def eliminar(id):
    db = conexion()
    resultado = db.licors.delete_one(
        {
        '_id': ObjectId(id)
        })
    return resultado

def opciones(opcion):
	if opcion == 1	:
		print("-----------------------------------------------------------------------")
		print("Agregaste un Licor")
		nombre = input("Nombre del licor: ")
		precio = int(input("Precio del licor: "))
		licor = Licor(nombre, precio)
		insertar(licor)
		print("El id del licor insertado es: ", ObjectId())
		print("-----------------------------------------------------------------------")
		print("Agregaste un Licor")
	elif opcion == 2:
		
		print("-----------------------------------------------------------------------")
		print("Estos son los Licores")
		print("-----------------------------------------------------------------------")
		for licor in obtener():
			print("Id: ", licor["_id"])
			print("Nombre Licor: ", licor["Nombre Licor"])
			print("Precio Licer: ", licor["Precio Licor"])
			print("-----------------------------------------------------------------------")
		print("Estos son los Licores")
	elif opcion == 3:
		print("-----------------------------------------------------------------------")
		print("Actualizaste un Licor")
		print("-----------------------------------------------------------------------")
		id = input("Cual es el ID del Licor: ")
		nombre = input("Nuevo nombre del Licor: ")
		precio = int(input("Nuevo precio del Licor: "))
		licor = Licor(nombre, precio)
		actualizar_licor = actualizar(id, licor)
		print("-----------------------------------------------------------------------")
		print("Actualizaste un Licor")
	elif opcion == 4:
		print("-----------------------------------------------------------------------")
		print("Eliminaste un Licor\n")
		print("-----------------------------------------------------------------------")
		id = input("Dime el id: ")
		eliminados_licor = eliminar(id)
		print("-----------------------------------------------------------------------")
		print("Eliminaste un Licor\n")
	elif opcion == 5:
		print("-----------------------------------------------------------------------")
		print("Programa Cerrado\n")
	else:
		print("-----------------------------------------------------------------------")
		print("Elige otra Opcion")

menu = """-------------------------------------------------------------------------
Bienvenido a la Licoreria.
1 - Insertar licor
2 - Ver todos
3 - Actualizar
4 - Eliminar
5 - Salir
"""

opcion = 0
a=1
while opcion != 5:
	print(menu)
	opcion = int(input("Elige: "))
	opciones(opcion)

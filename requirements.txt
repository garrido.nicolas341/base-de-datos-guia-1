dnspython==2.2.1
motor==2.5.1
pkg-resources==0.0.0
pymongo==3.12.3
pytz==2022.1
sqlparse==0.4.2
typing-extensions==4.1.1

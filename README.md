# Base de Datos - Guía 1

Desarrolle una simple aplicación escrita en Python que sea capaz de realizar las operaciones básicas CRUD sobre una colección en la BD.

## Clonación git

```
git clone https://gitlab.com/garrido.nicolas341/base-de-datos-guia-1.git
```

## Ejecución programa

Para iniciar el programa se debe iniciar los siguientes comandos:

```
cd base-de-datos-guia-1
pip install -r requirements.txt
source entorno3/bin/activate
cd entorno3 
cd inclide
python Guia-1.py 
```

## Ejemplo

```
Bienvenido a la Licoreria.
1 - Insertar licor
2 - Ver todos
3 - Actualizar
4 - Eliminar
5 - Salir
```
1. Inserta un licor dentro de la BD
2. Visualiza los datos de la BD
3. Actualiza algun licor dentro de la BD con la ID
4. Elimina algun licor dentro de la BD con la ID
5. Finaliza el programa
